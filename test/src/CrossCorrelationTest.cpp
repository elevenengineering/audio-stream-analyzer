// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#include <cstddef>
#include <type_traits>

#include <boost/test/unit_test.hpp>

#include "Algorithms.h"
#include "SineGenerator.h"
#include "PseudoRandomGenerator.h"

using namespace analyzer;

// should be a sufficiently large power-of-2, no less than 64
static std::size_t const kNumSamples = 512;
static std::size_t const kNumRandomTrials = 1000;
static AudioDftExecutor<kNumSamples> dft_exec;
static auto& in_buf = dft_exec.InputBuffer();

// Verify that the DFT and energy algorithms work to acceptable precision.
BOOST_AUTO_TEST_CASE(EnergyRandomTest)
{
  StdPseudoRandomGenerator<double> gen{-1, 1};
  gen.GetNextSamples(in_buf.data(), in_buf.size());
  auto const & result = dft_exec.Execute();

  // Verify that Parseval's Theorem holds.
  double const time_domain_energy = get_signal_energy(
    in_buf.begin(), in_buf.end());
  double const freq_domain_energy = get_reduced_freq_signal_energy(
    result.begin(), result.end());
  auto const normalized_delta =
    (time_domain_energy - freq_domain_energy) / time_domain_energy;
  BOOST_CHECK(std::abs(normalized_delta) < 1e-12);

  // Verify that our normalization functions work as intended.
  normalize_signal_by_energy(in_buf.begin(), in_buf.end(), in_buf.begin());
  auto const normalized_time_energy =
    get_signal_energy(in_buf.begin(), in_buf.end());
  BOOST_CHECK(std::abs(normalized_time_energy - 1) < 1e-12);

  decltype(dft_exec)::OutBufTy freq_buf;
  normalize_reduced_freq_signal_by_energy(
    result.begin(), result.end(), freq_buf.begin());
  auto const normalized_freq_energy =
    get_reduced_freq_signal_energy(freq_buf.begin(), freq_buf.end());
  BOOST_CHECK(std::abs(normalized_freq_energy - 1) < 1e-12);
}


// Verify the sine generator and that the DFT correctly finds its spectral power
// peak.
BOOST_AUTO_TEST_CASE(DftSineTest)
{
  // arbitrary, positive integer
  std::uint_fast32_t const kSampleRate = 48000;
  // arbitrary as long as it meets the Nyquist requirement
  StdPseudoRandomGenerator<double> freq_gen{
    kSampleRate * 1e-4, kSampleRate / 2.1};

  for (std::size_t i = 0; i < kNumRandomTrials; ++i) {
    auto const freq = freq_gen.GetNextSample();

    SineGenerator<double> gen{freq, kSampleRate};
    gen.GetNextSamples(in_buf.data(), in_buf.size());
    auto const & result = dft_exec.Execute();

    // Verify that the spectral power is maximized where we expect it to be for a
    // sine wave.
    auto comp_func = [&] (auto const & v0, auto const & v1) {
      return std::abs(v0) < std::abs(v1);
    };
    auto const max_it = std::max_element(result.begin(), result.end(), comp_func);
    auto const index = std::distance(result.begin(), max_it);
    BOOST_CHECK(std::abs(index - freq*kNumSamples/kSampleRate) < 1);
  }
}


// Verify the cross-correlation algorithm for the autocorrelation of a signal.
BOOST_AUTO_TEST_CASE(AutoCorrelationRandomTest)
{
  for (std::size_t i = 0; i < kNumRandomTrials; ++i) {
    StdPseudoRandomGenerator<double> gen{-1, 1};
    gen.GetNextSamples(in_buf.data(), in_buf.size());
    auto const & result = dft_exec.Execute();

    // Verify that the spectral energy overlap of a signal's autocorrelation is
    // equal to the signal's energy squared.
    auto spectral_overlap = get_cross_correlate_reduced_freq_energy_overlap(
      result.begin(), result.end(), result.begin());
    auto const time_domain_energy = get_signal_energy(
      in_buf.begin(), in_buf.end());
    auto const time_domain_energy_sq = time_domain_energy*time_domain_energy;

    auto const normalized_delta =
      (time_domain_energy_sq - spectral_overlap) / time_domain_energy_sq;
    BOOST_CHECK(std::abs(normalized_delta) < 1e-12);


    // Normalize the signal by energy, and verify that the autocorrelation
    // spectral energy is 1.
    normalize_signal_by_energy(in_buf.begin(), in_buf.end(), in_buf.begin());
    dft_exec.Execute();
    spectral_overlap = get_cross_correlate_reduced_freq_energy_overlap(
      result.begin(), result.end(), result.begin());
    BOOST_CHECK(std::abs(spectral_overlap - 1) < 1e-12);
  }
}


// Verify the cross-correlation algorithm for varying powers and noise levels of
// a signal.
BOOST_AUTO_TEST_CASE(CrossCorrelationSineTest)
{
  for (std::size_t i = 0; i < kNumRandomTrials; ++i) {
    // arbitrary, positive integer
    std::uint_fast32_t const kSampleRate = 48000;
    // arbitrary as long as it meets the Nyquist requirement and the DFT can
    // distinguish it well from DC
    StdPseudoRandomGenerator<double> freq_gen{
      2*kSampleRate/kNumSamples, kSampleRate / 2.1};

    // Generate a reference frequency sine signal.
    auto const ref_freq = freq_gen.GetNextSample();
    SineGenerator<double> ref_gen{ref_freq, kSampleRate};
    ref_gen.GetNextSamples(in_buf.data(), in_buf.size());
    auto& result = dft_exec.Execute();
    decltype(dft_exec)::OutBufTy ref_signal;
    std::copy(result.begin(), result.end(), ref_signal.begin());

    // Verify that a cross-correlation with a scaled version of the reference
    // yields a spectral energy overlap equal to the product of the two signals'
    // energy.
    auto const ref_sig_energy = get_reduced_freq_signal_energy(
      ref_signal.begin(), ref_signal.end());
    const double value_scalar = 0.5;
    for (auto& sample : in_buf) {
      sample *= value_scalar;
    }
    dft_exec.Execute();

    auto const time_domain_energy = get_signal_energy(
      in_buf.begin(), in_buf.end());
    auto const signal_energy = time_domain_energy*ref_sig_energy;
    auto const scaled_spectral_overlap =
      get_cross_correlate_reduced_freq_energy_overlap(
        result.begin(), result.end(), ref_signal.begin());
    auto const normalized_delta =
      (signal_energy - scaled_spectral_overlap) / signal_energy;
    BOOST_CHECK(std::abs(normalized_delta) < 1e-12);


    // Normalize our reference signal for further use.
    normalize_reduced_freq_signal_by_energy(
      ref_signal.begin(), ref_signal.end(), ref_signal.begin());


    // Verify that the cross-correlation of a phase-delayed reference with the
    // reference itself is almost equivalent to an autocorrelation.
    ref_gen.GetNextSamples(in_buf.data(), in_buf.size());
    dft_exec.Execute();
    normalize_reduced_freq_signal_by_energy(
      result.begin(), result.end(), result.begin());
    auto const autocorr_overlap = get_cross_correlate_reduced_freq_energy_overlap(
      result.begin(), result.end(), ref_signal.begin());
    BOOST_CHECK(1 - autocorr_overlap < 1e-1);


    // Verify that there is almost no cross-correlation between sinusoids of
    // considerably different frequency.
    auto new_signal_freq = freq_gen.GetNextSample();
    // Ensure that our signal frequencies are at least five DFT "bins" apart.
    while (std::abs(new_signal_freq - ref_freq) < 5*kSampleRate/kNumSamples) {
      new_signal_freq = freq_gen.GetNextSample();
    }
    SineGenerator<double> new_signal_gen{new_signal_freq, kSampleRate};
    new_signal_gen.GetNextSamples(in_buf.data(), in_buf.size());
    dft_exec.Execute();
    normalize_reduced_freq_signal_by_energy(
      result.begin(), result.end(), result.begin());
    auto const sine_crosscorr_overlap =
      get_cross_correlate_reduced_freq_energy_overlap(
        result.begin(), result.end(), ref_signal.begin());
    BOOST_CHECK(sine_crosscorr_overlap < 1e-1);


    // Verify that our cross-correlation with a mixed signal yields a
    // spectral overlap within expectations.
    decltype(dft_exec)::InBufTy mixed_signal;
    ref_gen.GetNextSamples(mixed_signal.data(), mixed_signal.size());
    auto sum_func = [&] (auto const v0, auto const v1) {
      return v0 + v1;
    };
    std::transform(
      in_buf.begin(), in_buf.end(), mixed_signal.begin(), in_buf.begin(), sum_func);
    normalize_signal_by_energy(in_buf.begin(), in_buf.end(), in_buf.begin());
    dft_exec.Execute();
    auto const mixed_crosscorr_overlap =
      get_cross_correlate_reduced_freq_energy_overlap(
        result.begin(), result.end(), ref_signal.begin());
    BOOST_CHECK(std::abs(mixed_crosscorr_overlap - 0.5) < 1.5e-1);
  }
}
