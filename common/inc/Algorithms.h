// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_ALGORITHMS_H
#define AUDIO_STREAM_ANALYZER_ALGORITHMS_H

#include <cstddef>
#include <cmath>
#include <complex>
#include <array>
#include <algorithm>
#include <numeric>
#include <exception>

#include <fftw3.h>

namespace analyzer {

template <std::size_t NumSamples>
class AudioDftExecutor
{
public:
  static const std::size_t NumDftSamples = NumSamples/2 + 1;
  using ComplexTy = std::complex<double>;
  using InBufTy = std::array<double, NumSamples>;
  using OutBufTy = std::array<ComplexTy, NumDftSamples>;


  AudioDftExecutor(void)
  {
    // Since our input is audio, we know that it is always real-valued and 1D.
    // This spares us the calculation of redundant values thanks to symmetry.
    this->plan_ = fftw_plan_dft_r2c_1d(
      NumSamples,
      this->in_buf_.data(),
      // fftw_complex and ComplexTy are bitwise-compatible
      reinterpret_cast<fftw_complex*>(this->out_buf_.data()),
      FFTW_MEASURE);
    if (this->plan_ == nullptr) {
      throw std::exception{};
    }
  }


  ~AudioDftExecutor(void)
  {
    fftw_destroy_plan(this->plan_);
  }


  InBufTy& InputBuffer(void) { return this->in_buf_; }


  OutBufTy& Execute(void)
  {
    fftw_execute(this->plan_);
    std::transform(
      this->out_buf_.begin(),
      this->out_buf_.end(),
      this->out_buf_.begin(),
      NormalizeDft);
    return this->out_buf_;
  }


  static ComplexTy NormalizeDft(ComplexTy const & v)
  {
    return v/std::sqrt(NumSamples);
  }

private:
  InBufTy in_buf_;
  OutBufTy out_buf_;
  fftw_plan plan_;
};


template <typename T>
auto energy_atom_function(T const v0, T const v1)
{
  return std::abs(v0*v1);
}


// Computes the energy of a signal.
template <typename InputIter>
auto get_signal_energy(InputIter begin, InputIter end)
{
  auto sum_func = [&] (auto const v0, auto const v1) {
    return v0 + v1;
  };
  auto prod_func = [&] (auto const v0, auto const v1) {
    return energy_atom_function(v0, v1);
  };
  return std::inner_product(begin, end, begin, 0.0, sum_func, prod_func);
}


// Computes the energy of a reduced frequency-domain signal from a
// fftw_plan_dft_r2c_1d.
template <typename InputIter>
auto get_reduced_freq_signal_energy(InputIter begin, InputIter end)
{
  auto const dc_coeff = *begin;
  auto const final_ac_coeff = *std::prev(end);
  auto const dc_energy = energy_atom_function(dc_coeff, dc_coeff);
  auto const final_ac_energy = energy_atom_function(final_ac_coeff, final_ac_coeff);
  auto const ac_energy = 2.0*get_signal_energy(
    std::next(begin), std::prev(end));
  return dc_energy + ac_energy + final_ac_energy;
}


template <typename InputIter, typename OutputIter, typename EnergyFunc>
void generic_normalize_signal_by_energy(
  InputIter begin, InputIter end, OutputIter out_begin, EnergyFunc func)
{
  auto const norm_factor = std::sqrt(func(begin, end));
  if (norm_factor == 0.0) {
    if (out_begin != begin) {
      std::copy(begin, end, out_begin);
    }
    return;
  }
  std::transform(
    begin, end, out_begin, [&] (auto const v) { return v / norm_factor; });
}


// Normalizes a time-domain or FULL frequency-domain signal so that its total
// energy is 1.
template <typename InputIter, typename OutputIter>
void normalize_signal_by_energy(
  InputIter begin, InputIter end, OutputIter out_begin)
{
  auto func = [&] (auto begin, auto end) {
    return get_signal_energy(begin, end);
  };
  return generic_normalize_signal_by_energy(begin, end, out_begin, func);
}


// Normalizes a reduced frequency-domain signal so that its total energy is 1.
template <typename InputIter, typename OutputIter>
void normalize_reduced_freq_signal_by_energy(
  InputIter begin, InputIter end, OutputIter out_begin)
{
  auto func = [&] (auto begin, auto end) {
    return get_reduced_freq_signal_energy(begin, end);
  };
  return generic_normalize_signal_by_energy(begin, end, out_begin, func);
}


template <typename T>
auto cross_correlation_atom_function(T const v0, T const v1)
{
  return v0*std::conj(v1);
}


// Computes the cross-correlation of 2 frequency-domain signals. They may be
// either full or reduced signals.
// Both signals must have the same length.
template <typename InputIter0, typename InputIter1, typename OutputIter>
OutputIter cross_correlate_freq(
  InputIter0 begin0, InputIter0 end0, InputIter1 begin1, OutputIter out_begin)
{
  return std::transform(
    begin0, end0, begin1, out_begin, cross_correlation_atom_function);
}


// Computes the spectral energy overlap of two reduced frequency-domain signals
// using their cross-correlation. This can be at most the square of the product
// of the signals' energy (if it is an autocorrelation), and no less than 0 if
// there is absolutely no overlap between signals.
// Both signals must have the same length.
template <typename InputIter0, typename InputIter1>
auto get_cross_correlate_reduced_freq_energy_overlap(
  InputIter0 begin0, InputIter0 end0, InputIter1 begin1)
{
  auto sum_func = [&] (auto const v0, auto const v1) {
    return v0 + v1;
  };
  auto prod_func = [&] (auto const v0, auto const v1) {
    return std::abs(cross_correlation_atom_function(v0, v1));
  };
  auto const end1 = std::next(begin1, std::distance(begin0, end0));
  auto const dc_overlap = prod_func(*begin0, *begin1);
  auto const final_ac_overlap = prod_func(*std::prev(end0), *std::prev(end1));
  auto const ac_overlap = 2.0*std::inner_product(
    std::next(begin0),
    std::prev(end0),
    std::next(begin1),
    0.0,
    sum_func,
    prod_func);
  auto const total_sum = dc_overlap + final_ac_overlap + ac_overlap;
  return energy_atom_function(total_sum, total_sum);
}

} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_ALGORITHMS_H
