// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_SINE_GEN_H
#define AUDIO_STREAM_ANALYZER_SINE_GEN_H

#include <cstddef>
#include <cmath>

#include "SampleSource.h"

namespace analyzer {

// Generates a discrete sequence of floating-point sinusoidal values
// corresponding to the provided frequency and sample rate.
// Subject to the Nyquist sampling theorem, and will generated an aliased
// sequence if the frequency is greater than 2*sample_rate.
template <typename T>
class SineGenerator :
  public SampleSource<SineGenerator<T>, T>
{
public:
  constexpr SineGenerator(
      double frequency,
      double sample_rate,
      T scalar = 1,
      double initial_angle = 0) :
    scalar_{scalar},
    index_angle_increment_{2*M_PI*frequency/sample_rate},
    curr_angle_{initial_angle}
  {}

  T GetNextSample(void)
  {
    T const result = this->scalar_ * std::sin(this->curr_angle_);
    this->curr_angle_ += this->index_angle_increment_;
    return result;
  }

  void GetNextSamplesImpl(T* buf, std::size_t len)
  {
    for (std::size_t i = 0; i < len; ++i) {
      buf[i] = this->GetNextSample();
    }
    // This prevents curr_angle_ from growing without bound while retaining a
    // contiguous change in phase angle.
    this->curr_angle_ = std::fmod(this->curr_angle_, 2*M_PI);
  }

  T const scalar_;
  double const index_angle_increment_;
  double curr_angle_;
};

} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_SINE_GEN_H
