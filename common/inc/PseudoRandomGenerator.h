// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_PSEUDO_RANDOM_GEN_H
#define AUDIO_STREAM_ANALYZER_PSEUDO_RANDOM_GEN_H

#include <cstddef>
#include <random>

#include "SampleSource.h"

namespace analyzer {

// Generates a discrete sequence of pseudo-random values.
template <typename PrngTy, typename T>
class PseudoRandomGenerator :
  public SampleSource<PseudoRandomGenerator<PrngTy, T>, T>
{
public:
  constexpr PseudoRandomGenerator(T min, T max) : min_{min}, max_{max} {}

  T GetNextSample(void)
  {
    double const proportion =
      static_cast<double>(this->prng_() - PrngTy::min()) /
      static_cast<double>(PrngTy::max() - PrngTy::min());
    return this->min_ + proportion*(this->max_ - this->min_);
  }

  void GetNextSamplesImpl(T* buf, std::size_t len)
  {
    for (std::size_t i = 0; i < len; ++i) {
      buf[i] = this->GetNextSample();
    }
  }

private:
  T const min_;
  T const max_;
  PrngTy prng_{std::random_device{}()};
};

template <typename T>
using StdPseudoRandomGenerator = PseudoRandomGenerator<std::minstd_rand, T>;

} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_PSEUDO_RANDOM_GEN_H
