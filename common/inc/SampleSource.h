// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_SAMPLE_SOURCE_H
#define AUDIO_STREAM_ANALYZER_SAMPLE_SOURCE_H

#include <cstddef>

namespace analyzer {

template <class Derived, typename T>
class SampleSource
{
public:
  void GetNextSamples(T* buf, std::size_t len)
  {
    return static_cast<Derived*>(this)->GetNextSamplesImpl(buf, len);
  }
};

} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_SAMPLE_SOURCE_H
