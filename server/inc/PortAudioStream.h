// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_PORTAUDIO_STREAM_H
#define AUDIO_STREAM_ANALYZER_PORTAUDIO_STREAM_H

#include <portaudio.h>

#include "PortAudioException.h"
#include "SampleSource.h"

namespace analyzer {
namespace portaudio {

// FIXME: Enforce this as only being constructable from a Context.
template <bool IsOut>
class BaseStream
{
public:
  BaseStream(
    PaDeviceIndex device,
    int num_channels,
    double sample_rate,
    unsigned long frames_per_buffer,
    PaStreamCallback callback,
    void* callback_data)
  {
    PaStreamParameters params;
    params.device = device;
    params.channelCount = num_channels;
    params.sampleFormat = paFloat32;
    params.suggestedLatency = 0.1; // this latency is quite arbitrary
    params.hostApiSpecificStreamInfo = nullptr;

    auto const err = Pa_OpenStream(
      &this->raw_stream_,
      IsOut ? nullptr : &params,
      !IsOut ? nullptr : &params,
      sample_rate,
      frames_per_buffer,
      paNoFlag,
      callback,
      callback_data);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  ~BaseStream(void)
  {
    try {
      this->Abort();
    }
    catch (std::exception const &) {}
    Pa_CloseStream(this->raw_stream_);
  }


  void Start(void)
  {
    auto const err =  Pa_StartStream(this->raw_stream_);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  void Stop(void)
  {
    auto const err = Pa_StopStream(this->raw_stream_);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  void Abort(void)
  {
    auto const err = Pa_AbortStream(this->raw_stream_);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  PaStreamInfo const * GetInfo(void)
  {
    return Pa_GetStreamInfo(this->raw_stream_);
  }


protected:
  PaStream* raw_stream_;
};


template <bool IsOut>
class CallbackStream : public BaseStream<IsOut>
{
public:
  CallbackStream(
      PaDeviceIndex device,
      int num_channels,
      double sample_rate,
      unsigned long frames_per_buffer,
      PaStreamCallback callback,
      void* callback_data) :
    BaseStream<IsOut>{
      device,
      num_channels,
      sample_rate,
      frames_per_buffer,
      callback,
      callback_data}
  {}
};

using OutCallbackStream = CallbackStream<true>;
using InCallbackStream = CallbackStream<false>;


class OutStream : public BaseStream<true>
{
public:
  OutStream(
      PaDeviceIndex device,
      int num_channels,
      double sample_rate,
      unsigned long frames_per_buffer) :
    BaseStream<true>{
      device, num_channels, sample_rate, frames_per_buffer, nullptr, nullptr}
  {}


  void Write(float const * buf, unsigned long frames)
  {
    auto const err = Pa_WriteStream(this->raw_stream_, buf, frames);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  signed long GetWriteAvailable(void)
  {
    auto const ret = Pa_GetStreamWriteAvailable(this->raw_stream_);
    if (ret < 0) {
      throw Exception{static_cast<PaError>(ret)};
    }
    return ret;
  }
};


class InStream :
  public BaseStream<false>, public SampleSource<InStream, float>
{
public:
  InStream(
      PaDeviceIndex device,
      int num_channels,
      double sample_rate,
      unsigned long frames_per_buffer) :
    BaseStream<false>{
      device, num_channels, sample_rate, frames_per_buffer, nullptr, nullptr}
  {}


  signed long GetReadAvailable(void)
  {
    auto const ret = Pa_GetStreamReadAvailable(this->raw_stream_);
    if (ret < 0) {
      throw Exception{static_cast<PaError>(ret)};
    }
    return ret;
  }


  void Read(float* buf, unsigned long frames)
  {
    auto const err = Pa_ReadStream(this->raw_stream_, buf, frames);
    if (err != PaErrorCode::paNoError) {
      throw Exception{err};
    }
  }


  void GetNextSamplesImpl(float* buf, std::size_t len)
  {
    this->Read(buf, len);
  }
};

} // end namespace portaudio
} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_PORTAUDIO_STREAM_H
