// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#ifndef AUDIO_STREAM_ANALYZER_PORTAUDIO_CONTEXT_H
#define AUDIO_STREAM_ANALYZER_PORTAUDIO_CONTEXT_H

#include <portaudio.h>

#include "PortAudioStream.h"

namespace analyzer {
namespace portaudio {

// FIXME: Enforce this as a singleton.
class Context
{
public:
  Context(void);
  ~Context(void);

  InStream OpenInStream(
    PaDeviceIndex device,
    int num_channels,
    double sample_rate,
    unsigned long frames_per_buffer);

  OutStream OpenOutStream(
    PaDeviceIndex device,
    int num_channels,
    double sample_rate,
    unsigned long frames_per_buffer);

  InCallbackStream OpenInCallbackStream(
    PaDeviceIndex device,
    int num_channels,
    double sample_rate,
    unsigned long frames_per_buffer,
    PaStreamCallback callback,
    void* callback_data);

  OutCallbackStream OpenOutCallbackStream(
    PaDeviceIndex device,
    int num_channels,
    double sample_rate,
    unsigned long frames_per_buffer,
    PaStreamCallback callback,
    void* callback_data);

  PaDeviceIndex GetDeviceCount(void);
  PaDeviceInfo const * GetDeviceInfo(PaDeviceIndex index);
};

} // end namespace portaudio
} // end namespace analyzer
#endif // AUDIO_STREAM_ANALYZER_PORTAUDIO_CONTEXT_H
