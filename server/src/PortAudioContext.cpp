// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#include "PortAudioContext.h"
#include "PortAudioException.h"

namespace analyzer {
namespace portaudio {

Context::Context(void)
{
  auto const err = Pa_Initialize();
  if (err != PaErrorCode::paNoError) {
    throw Exception{err};
  }
}

Context::~Context(void) { Pa_Terminate(); }


PaDeviceIndex Context::GetDeviceCount(void)
{
  return Pa_GetDeviceCount();
}


PaDeviceInfo const * Context::GetDeviceInfo(PaDeviceIndex index)
{
  return Pa_GetDeviceInfo(index);
}

InStream Context::OpenInStream(
  PaDeviceIndex device,
  int num_channels,
  double sample_rate,
  unsigned long frames_per_buffer)
{
  return InStream{device, num_channels, sample_rate, frames_per_buffer};
}

OutStream Context::OpenOutStream(
  PaDeviceIndex device,
  int num_channels,
  double sample_rate,
  unsigned long frames_per_buffer)
{
  return OutStream{device, num_channels, sample_rate, frames_per_buffer};
}

InCallbackStream Context::OpenInCallbackStream(
  PaDeviceIndex device,
  int num_channels,
  double sample_rate,
  unsigned long frames_per_buffer,
  PaStreamCallback callback,
  void* callback_data)
{
  return InCallbackStream{
    device, num_channels, sample_rate, frames_per_buffer, callback, callback_data};
}

OutCallbackStream Context::OpenOutCallbackStream(
  PaDeviceIndex device,
  int num_channels,
  double sample_rate,
  unsigned long frames_per_buffer,
  PaStreamCallback callback,
  void* callback_data)
{
  return OutCallbackStream{
    device, num_channels, sample_rate, frames_per_buffer, callback, callback_data};
}

} // end namespace portaudio
} // end namespace analyzer
