// Copyright © 2017 Eleven Engineering Inc. Thia Wyrod <wyrod@eleveneng.com>
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA

#include <array>
#include <utility>
#include <functional>
#include <csignal>
#include <iostream>

#include <boost/chrono.hpp>
#include <boost/thread.hpp>

#include <docopt.h>

#include "PortAudioContext.h"
#include "PortAudioException.h"
#include "PortAudioStream.h"
#include "SineGenerator.h"
#include "Algorithms.h"

using namespace analyzer;

// gives us reasonable DFT frequency resolution
static const unsigned long kFramesPerBuffer = 512;


static bool continue_running_program = false;

static void term_process_handler(int)
{
  continue_running_program = false;
}


void print_audio_streams(portaudio::Context& context)
{
  auto const num_devices = context.GetDeviceCount();
  for (auto i = 0; i != num_devices; ++i) {
    auto const * dev = context.GetDeviceInfo(i);
    std::cout << "Index: " << i << '\n';
    std::cout << "Name: " << dev->name << '\n';
    std::cout << "Default Sample Rate: " << dev->defaultSampleRate << '\n';
    std::cout << "Output Channels: " << dev->maxOutputChannels << '\n';
    std::cout << "Input Channels: " << dev->maxInputChannels << '\n';
    std::cout << '\n';
  }
}


// Yes, this has a nasty, C-typical signature, but PortAudio demands it.
int tone_to_out_stream_callback(
  void const *,
  void* output,
  unsigned long num_frames,
  PaStreamCallbackTimeInfo const *,
  PaStreamCallbackFlags,
  void* data)
{
  auto* gen = reinterpret_cast<SineGenerator<float>*>(data);
  gen->GetNextSamples(reinterpret_cast<float*>(output), num_frames);
  return PaStreamCallbackResult::paContinue;
}


struct in_stream_to_analysis_callback_data
{
  in_stream_to_analysis_callback_data(
    double aux_out_freq, double bt_freq, double sample_rate)
  {
    auto& in_buf = this->dft_exec.InputBuffer();

    SineGenerator<double> aux_gen{aux_out_freq, sample_rate};
    aux_gen.GetNextSamples(in_buf.data(), in_buf.size());
    auto const & result = dft_exec.Execute();
    normalize_reduced_freq_signal_by_energy(
      result.begin(), result.end(), aux_ref.begin());

    SineGenerator<double> bt_gen{bt_freq, sample_rate};
    bt_gen.GetNextSamples(in_buf.data(), in_buf.size());
    dft_exec.Execute();
    normalize_reduced_freq_signal_by_energy(
      result.begin(), result.end(), bt_ref.begin());
  }

  AudioDftExecutor<kFramesPerBuffer> dft_exec;
  decltype(dft_exec)::OutBufTy aux_ref;
  decltype(dft_exec)::OutBufTy bt_ref;
  double aux_out_freq_correlation = 0.0;
  double bt_freq_correlation = 0.0;
};


int in_stream_to_analysis_callback(
  void const * input,
  void*,
  unsigned long num_frames,
  PaStreamCallbackTimeInfo const *,
  PaStreamCallbackFlags,
  void* raw_data)
{
  auto* data = reinterpret_cast<in_stream_to_analysis_callback_data*>(raw_data);
  auto& in_buf = data->dft_exec.InputBuffer();
  auto const * begin = reinterpret_cast<float const *>(input);
  auto const * end = begin + num_frames;
  std::copy(begin, end, in_buf.begin());
  auto& result = data->dft_exec.Execute();
  normalize_reduced_freq_signal_by_energy(
    result.begin(), result.end(), result.begin());
  data->aux_out_freq_correlation = get_cross_correlate_reduced_freq_energy_overlap(
    result.begin(), result.end(), data->aux_ref.begin());
  data->bt_freq_correlation = get_cross_correlate_reduced_freq_energy_overlap(
    result.begin(), result.end(), data->bt_ref.begin());
  return PaStreamCallbackResult::paContinue;
}


using DocoptCheckFuncTy = bool(docopt::value::*)(void) const;

static const std::array<std::pair<std::string, DocoptCheckFuncTy>, 6> kArgChecks = {{
  {"--list-streams", &docopt::value::isLong},
  {"--bluetooth-out-stream", &docopt::value::isLong},
  {"--aux-out-stream", &docopt::value::isLong},
  {"--aux-in-stream", &docopt::value::isLong},
  {"--bluetooth-out-frequency", &docopt::value::isLong},
  {"--aux-out-frequency", &docopt::value::isLong},
}};


static const char kUsageStr[] =
R"(audio-stream-analyzer-server

Usage:
  audio-stream-analyzer-server --list-streams
  audio-stream-analyzer-server --aux-out-stream=<stream> --bluetooth-out-stream=<stream> --aux-in-stream=<stream> --aux-out-frequency=<freq> --bluetooth-out-frequency=<freq> [--sample-rate=<rate>]
  audio-stream-analyzer-server (-h | --help)
  audio-stream-analyzer-server --version

Options:
  --list-streams                    Get info of all currently present audio
                                    streams in the system.
  --bluetooth-out-stream=<stream>   Index of bluetooth output stream.
  --aux-out-stream=<stream>         Index of aux out stream.
  --aux-in-stream=<stream>          Index of aux in stream.
  --bluetooth-out-frequency=<freq>  Frequency of bluetooth out tone.
  --aux-out-frequency=<freq>        Frequency of aux out tone.
  --sample-rate=<rate>              Sample rate to use for all streams. Defaults
                                    to the default sample rate of the aux-out
                                    stream.
  -h --help                         Show this usage information.
  --version                         Show version.
)";


bool check_arg_types(std::map<std::string, docopt::value> const & args)
{
  bool all_ok = true;
  for (auto const & pair : kArgChecks) {
    auto const & arg = args.at(pair.first);
    if (!arg) { // the argument wasn't passed in on the command line
      continue;
    }
    if ((arg.*pair.second)()) {
      std::cerr << pair.first << " was invoked with an incorrect argument type.\n";
      all_ok = false;
    }
  }
  return all_ok;
}


void run_server(
  portaudio::Context& context,
  std::array<PaDeviceIndex, 3>& devices,
  double aux_out_freq,
  double bt_freq,
  double sample_rate)
{
  SineGenerator<float> aux_out_gen{aux_out_freq, sample_rate};
  auto aux_out_stream = context.OpenOutCallbackStream(
    devices[0],
    1,
    sample_rate,
    kFramesPerBuffer,
    tone_to_out_stream_callback,
    &aux_out_gen);

  SineGenerator<float> bt_gen{bt_freq, sample_rate};
  auto bt_stream = context.OpenOutCallbackStream(
    devices[1],
    1,
    sample_rate,
    kFramesPerBuffer,
    tone_to_out_stream_callback,
    &bt_gen);

  in_stream_to_analysis_callback_data analyzer{
    aux_out_freq, bt_freq, sample_rate};
  auto aux_in_stream = context.OpenInCallbackStream(
    devices[2],
    1,
    sample_rate,
    kFramesPerBuffer,
    in_stream_to_analysis_callback,
    &analyzer);

  aux_out_stream.Start();
  bt_stream.Start();
  aux_in_stream.Start();

  while (continue_running_program) {
    std::cout << "Aux out probability: " << analyzer.aux_out_freq_correlation <<
      '\n';
    std::cout << "Bluetooth probability: " << analyzer.bt_freq_correlation <<
      '\n';
    boost::this_thread::sleep_for(boost::chrono::milliseconds{500});
  }
}


int main(int argc, char const ** argv)
{
  auto const args = docopt::docopt(
    kUsageStr, {argv + 1, argv + argc}, true, "v0.1.0");
  if (!check_arg_types(args)) {
    return -1;
  }

  signal(SIGINT, &term_process_handler);
  signal(SIGTERM, &term_process_handler);

  try {
    portaudio::Context context;

    if (args.at("--list-streams").asBool()) {
      print_audio_streams(context);
      return 0;
    }

    std::array<PaDeviceIndex, 3> devices;
    devices[0] = args.at("--aux-out-stream").asLong();
    devices[1] = args.at("--bluetooth-out-stream").asLong();
    devices[2] = args.at("--aux-in-stream").asLong();
    auto const device_count = context.GetDeviceCount();
    for (auto device : devices) {
      if (device >= device_count) {
        throw portaudio::Exception{PaErrorCode::paInvalidDevice};
      }
    }

    auto const sample_rate_arg = args.at("--sample-rate");
    double const sample_rate = sample_rate_arg ? sample_rate_arg.asLong() :
      context.GetDeviceInfo(*devices.begin())->defaultSampleRate;

    continue_running_program = true;
    run_server(
      context,
      devices,
      args.at("--aux-out-frequency").asLong(),
      args.at("--bluetooth-out-frequency").asLong(),
      sample_rate);
  }
  catch (std::exception const & e) {
    std::cerr << typeid(e).name() << ": " << e.what() << '\n';
    continue_running_program = false;
  }

  return 0;
}
